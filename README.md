# Quick Start
All information to start and use the applications should be written here.


**To install dependencies in "first" and "second" folders run:**

- composer install

**To run docker**

- docker-compose up -d --build

######  run bellows commands

- docker exec -it kayer_app_1 bash
- php artisan migrate --seed


######  Postman collection also exist in postman folder
- import kayer.postman_collection.json
