<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile',11)->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            // I implement it in simplest approach, But We can also implement role & permission or use some package like Spatie
            $table->tinyInteger('role')->default(2);//0 = admin | 1 = employee | 2 = customer
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
