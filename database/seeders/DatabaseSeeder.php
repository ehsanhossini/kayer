<?php

namespace Database\Seeders;

use App\Models\TicketCategory;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users=[
          [
              'name' => 'admin',
              'email' => 'admin@gmail.com',
              'role' => 0,
              'password' => bcrypt('admin123')
          ] ,
            [
              'name' => 'employee1',
              'email' => 'employee1@gmail.com',
              'role' => 1,
              'password' => bcrypt('employee123')
          ] ,
        ];
        foreach ($users as $user){
            User::create([
                'name' => $user['name'],
                'email' => $user['email'],
                'role' => $user['role'],
                'password' => $user['password']
            ]);
        }

        $ticketCategory=[
          ['name'=>'Technical','slug'=>'technical','is_visible'=>1],
          ['name'=>'Sale','slug'=>'sale','is_visible'=>1],
          ['name'=>'Financial','slug'=>'financial','is_visible'=>1],
        ];
        foreach ($ticketCategory as $category){
            TicketCategory::create([
                'name'=>$category['name'],
                'slug'=>$category['slug'],
                'is_visible'=>$category['is_visible']
            ]);
        }

    }
}
