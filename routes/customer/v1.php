<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\customer\CustomerController;

Route::prefix('/customer')->group(function () {
    Route::middleware(['auth:sanctum','user-role:customer'])->group(function () {
        Route::post('ticket', [CustomerController::class, 'createTicket'])->name('customer-create-ticket');
        Route::put('ticket/{uuid}', [CustomerController::class, 'updateTicket'])->name('customer-update-ticket');
        Route::put('ticket/reply/{uuid}', [CustomerController::class, 'replyTicket'])->name('customer-reply-ticket');
        Route::get('tickets', [CustomerController::class, 'ticketList'])->name('customer-ticket-list');
    });
});
