<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\employee\EmployeeController;

Route::prefix('/employee')->group(function () {
    Route::middleware(['auth:sanctum','user-role:employee'])->group(function () {
        Route::post('ticket', [EmployeeController::class, 'createTicket'])->name('employee-create-ticket');
        Route::put('ticket/reply/{uuid}', [EmployeeController::class, 'replyTicket'])->name('employee-reply-ticket');
        Route::get('tickets', [EmployeeController::class, 'ticketList'])->name('employee-ticket-list');
    });
});
