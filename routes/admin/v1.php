<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\admin\UserController;
use \App\Http\Controllers\admin\TicketController;

Route::prefix('/admin')->group(function () {
    Route::middleware(['auth:sanctum','user-role:admin'])->group(function () {
        Route::post('user', [UserController::class, 'createUser'])->name('admin-create-user');
        Route::put('user/{userId}', [UserController::class, 'updateUser'])->name('admin-update-user');
        Route::delete('user/{userId}', [UserController::class, 'deleteUser'])->name('admin-delete-user');
        Route::post('ticket', [TicketController::class, 'createTicket'])->name('admin-create-ticket');
        Route::put('ticket/{uuid}', [TicketController::class, 'updateTicket'])->name('admin-update-ticket');
        Route::delete('ticket/{uuid}', [TicketController::class, 'deleteTicket'])->name('admin-delete-ticket');
    });
});
