<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('/')->group(function () {
    // Authentication Routes
    include __DIR__ . '/auth/v1.php';
    include __DIR__ . '/admin/v1.php';
    include __DIR__ . '/customer/v1.php';
    include __DIR__ . '/employee/v1.php';
});
