<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;


class AuthValidationRule implements Rule
{

    public function __construct(private $email = null,private $password = null)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!is_null($this->email) && !is_null($this->password)){
            if(Auth::attempt(['email'=>$this->email, 'password'=>$this->password])){
                return true;
            }
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'incorrect credentials.';
    }
}
