<?php

namespace App\Rules;

use App\Models\Ticket;
use Illuminate\Contracts\Validation\Rule;



class ReplyTicketRule implements Rule
{
    /**
     * ReplyTicketRule constructor.
     * @param string $ticketId
     */
    public function __construct(private string $ticketId){

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(Ticket::where(['id'=>$this->ticketId,'user_id'=>auth()->user()->id])->first()){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'incorrect ticket.';
    }
}
