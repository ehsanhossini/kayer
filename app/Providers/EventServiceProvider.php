<?php

namespace App\Providers;

use App\Events\SendEmailEvent;
use App\Events\SendNotificationEvent;
use App\Events\SendSmsEvent;
use App\Listeners\SendEmailListener;
use App\Listeners\SendNotificationListener;
use App\Listeners\SendSmsListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendEmailEvent::class => [
            SendEmailListener::class,
        ],
        SendNotificationEvent::class => [
            SendNotificationListener::class,
        ],
        SendSmsEvent::class => [
            SendSmsListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
