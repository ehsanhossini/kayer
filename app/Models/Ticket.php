<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use HasFactory,HasUuids,SoftDeletes;

    const STATUS_OPEN = 'open';
    const STATUS_CLOSE = 'close';

    public static function boot()
    {
        parent::boot();

        static::created(function ($query) {
            $user = User::withCount('ticket')->employee()->orderBy('ticket_count', 'asc')->get();
            TicketUser::create([
                'user_id'=>$user->first()->id,
                'ticket_id'=>$query->id
            ]);
        });
    }

    protected $fillable=['ticket_category_id','user_id','title','priority','status','is_resolved'];

    public function user()
    {
        return $this->belongsTo(TicketUser::class,'id','ticket_id');
    }

    public function category()
    {
        return $this->belongsTo(TicketCategory::class,'ticket_category_id','id');
    }

    public function message()
    {
        return $this->hasMany(TicketMessage::class,'ticket_id','id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
