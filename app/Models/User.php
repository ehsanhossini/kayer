<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable ;

    const ROLE_ADMIN = 0;
    const ROLE_EMPLOYEE = 1;
    const ROLE_CUSTOMER = 2;

    protected $appends = [ 'user_role_title' ,'user_role_number'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'mobile'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function role(): Attribute
    {
        return new Attribute(
            get: fn($value) => ["admin", "employee", "customer"][$value],
        );
    }

    protected function getUserRoleTitleAttribute()
    {
        switch ($this->role) {
            case 'admin':
                return "مدیر";
            case 'employee':
                return 'کارمند';
            case 'customer':
                return 'مشتری';
        }
    }

    protected function getUserRoleNumberAttribute()
    {
        switch ($this->role) {
            case 'admin':
                return 1;
            case 'employee':
                return 2;
            case 'customer':
                return 3;
        }
    }

    public function ticket()
    {
        return $this->hasMany(TicketUser::class,'user_id','id');
    }

    public function scopeEmployee($query)
    {
        return $query->where('role',1);
    }

}
