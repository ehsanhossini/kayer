<?php

namespace App\Models;

use App\Events\SendEmailEvent;
use App\Events\SendNotificationEvent;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    use HasFactory,HasUuids;

    protected $fillable=['ticket_id','user_id','message'];


    public static function boot()
    {
        parent::boot();

        static::created(function ($query) {
            //if current user is not employer him/her self should send email
            if(auth()->user()->id != $query->employee->user->id){
                event(new SendEmailEvent([
                    'email'=>$query->employee->user->email,
                    'message'=>$query->message
                ]));
                event(new SendNotificationEvent([
                    'ticket_id'=>$query->ticket_id,
                    'message'=>$query->message
                ]));
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function ticket()
    {
        return $this->belongsTo(Ticket::class,'ticket_id','id');
    }

    public function employee()
    {
        return $this->belongsTo(TicketUser::class,'ticket_id','ticket_id');

    }
}
