<?php

namespace App\Repositories;

use App\Models\Ticket;
use App\Models\TicketMessage;
use App\Models\TicketUser;
use App\Models\User;

class EmployeeRepository
{
    /**
     * @param string $ticketCatId
     * @param string $title
     * @param string $message
     * @param int $priority
     */
    public function createTicket(string $ticketCatId,string $title,string $message,int $priority )
    {
        $ticket = Ticket::create([
            'ticket_category_id'=>$ticketCatId,
            'user_id'=>auth()->user()->id,
            'title'=>$title,
            'priority'=>$priority,
        ]);
        TicketMessage::create([
            'ticket_id'=>$ticket->id,
            'user_id'=>\auth()->user()->id,
            'message'=>$message
        ]);
        return $ticket;

    }

    /**
     * @param string $ticketId
     * @param string $message
     */
    public function replyTicket(string $ticketId,string $message)
    {
        $ticket = Ticket::find($ticketId);
            TicketMessage::create([
                'ticket_id'=>$ticket->id,
                'user_id'=>\auth()->user()->id,
                'message'=>$message
            ]);
    }

    /**
     * @param string $ticketId
     * @param int|null $priority
     * @param string|null $status
     * @param int|null $isResolved
     */
    public function updateTicket(string $ticketId,int $priority =null,string $status=null,int $isResolved=null)
    {

        if($ticket = Ticket::where('id',$ticketId)->whereHas('user',function ($q){
            return $q->where('user_id',\auth()->user()->id);
        })->first()){
            $ticket->update([
                'priority'=>$priority ?? $this->priority,
                'status'=>$status ?? $ticket->status,
                'is_resolved'=>$isResolved ??$ticket->is_resolved
            ]);
        }
    }

    public function ticketList()
    {
        return  Ticket::all();
    }


}
