<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository
{
    public function find($id)
    {
        return User::find($id);
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function create(string $name,string $email,string $password,$mobile): User
    {
        return User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'mobile' => $mobile
        ]);
    }

    public function login(string $email,string $password)
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = User::where('email', $email)->first();
            return $user->createToken(config('kayer.access_token'))->plainTextToken;
        }
    }

    public function logout()
    {
        Auth::logout();
    }

}
