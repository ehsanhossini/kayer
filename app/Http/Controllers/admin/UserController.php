<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\AdminRepository;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * Create User
     * @method POST
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUser(CreateUserRequest $request)
    {
        resolve(AdminRepository::class)->create($request->name,$request->email,$request->password,$request->role,
            $request->mobile);
        return response()->json(['message' => 'user created successfully'], Response::HTTP_CREATED);
    }

    /**
     * Update User
     * @method POST
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser($userId,UpdateUserRequest $request)
    {
        resolve(AdminRepository::class)->updateUser($userId,$request->role,$request->email,
            $request->name, $request->password);
        return response()->json(['message' => 'user updated successfully'], Response::HTTP_CREATED);
    }

    /**
     * Delete User
     * @method DELETE
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($userId)
    {
        resolve(AdminRepository::class)->deleteUser($userId);
        return response()->json(['message' => 'user deleted successfully'], Response::HTTP_CREATED);
    }



}
