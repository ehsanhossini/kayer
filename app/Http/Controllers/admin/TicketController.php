<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Repositories\AdminRepository;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends Controller
{
    /**
     * Create Ticket
     * @method POST
     * @param CreateTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTicket(CreateTicketRequest $request)
    {
        resolve(AdminRepository::class)->createTicket(
            $request->ticket_category_id,$request->title,$request->message,$request->priority ?? 0);
        return response()->json(['message' => 'ticket created successfully'], Response::HTTP_CREATED);
    }

    /**
     * Reply Ticket
     * @method PUT
     * @param UpdateTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTicket(UpdateTicketRequest $request)
    {
        resolve(AdminRepository::class)->updateTicket($request->route('uuid'),$request->priority,
            $request->status,$request->is_resolved);
        return response()->json(['message' => 'ticket updated successfully'], Response::HTTP_CREATED);
    }


    public function deleteTicket($uuid)
    {
        resolve(AdminRepository::class)->deleteTicket($uuid);
        return response()->json(['message' => 'ticket deleted successfully'], Response::HTTP_CREATED);
    }
}
