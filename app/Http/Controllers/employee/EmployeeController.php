<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\customer\ReplyTicketRequest;
use App\Http\Resources\TicketResource;
use App\Repositories\EmployeeRepository;
use Symfony\Component\HttpFoundation\Response;

class EmployeeController extends Controller
{
    /**
     * Reply Ticket
     * @method GET
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticketList()
    {
        $data = resolve(EmployeeRepository::class)->ticketList();
        return response()->json(['message' => 'Ticket list',
            'data'=>TicketResource::collection($data)
        ], Response::HTTP_OK);

    }

    /**
     * Create Ticket
     * @method POST
     * @param CreateTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTicket(CreateTicketRequest $request)
    {
        resolve(EmployeeRepository::class)->createTicket(
            $request->ticket_category_id,$request->title,$request->message,$request->priority ?? 0);
        return response()->json(['message' => 'ticket created successfully'], Response::HTTP_CREATED);
    }

    /**
     * Reply Ticket
     * @method POST
     * @param ReplyTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function replyTicket(ReplyTicketRequest  $request)
    {
        resolve(EmployeeRepository::class)->replyTicket($request->route('uuid'),$request->message);
        return response()->json(['message' => 'ticket replied successfully'], Response::HTTP_CREATED);
    }

}
