<?php

namespace App\Http\Controllers\auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{

    /**
     * Register New User
     * @method POST
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        // Insert User Into Database
        resolve(UserRepository::class)->create($request->name,$request->email,$request->password,
            $request->mobile);
        return response()->json(['message' => 'user created successfully'], Response::HTTP_CREATED);
    }

    /**
     * Login User
     * @method POST
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
         $data = resolve(UserRepository::class)->login($request->email,$request->password);
            return response()->json(['message' => 'user Logged in successfully',
                'token' => $data], Response::HTTP_OK);
    }

    /**
     * Logout User
     * @method GET
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        resolve(UserRepository::class)->logout();
        return response()->json(['message' => 'logged out successfully']);
    }

}
