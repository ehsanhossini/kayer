<?php

namespace App\Http\Controllers\customer;

use App\Events\SendEmailEvent;
use App\Events\SendNotificationEvent;
use App\Events\SendSmsEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\customer\UpdateTicketRequest;
use App\Http\Requests\customer\ReplyTicketRequest;
use App\Http\Resources\TicketResource;
use App\Repositories\CustomerRepository;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /**
     * Create Ticket
     * @method POST
     * @param CreateTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTicket(CreateTicketRequest $request)
    {
        $ticket = resolve(CustomerRepository::class)->createTicket(
            $request->ticket_category_id,$request->title,$request->message,$request->priority ?? 0);

        event(new SendSmsEvent([
            'customer_mobile'=>$ticket->customer->mobile,
            'message'=>$request->message
        ]));
        return response()->json(['message' => 'ticket created successfully'], Response::HTTP_CREATED);
    }

    /**
     * Reply Ticket
     * @method PUT
     * @param UpdateTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTicket(UpdateTicketRequest $request)
    {
        resolve(CustomerRepository::class)->updateTicket($request->route('uuid'),$request->priority,
        $request->status,$request->is_resolved);
        return response()->json(['message' => 'ticket updated successfully'], Response::HTTP_CREATED);
    }

    /**
     * Reply Ticket
     * @method POST
     * @param ReplyTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function replyTicket(ReplyTicketRequest  $request)
    {
        resolve(CustomerRepository::class)->replyTicket($request->route('uuid'),$request->message);
        return response()->json(['message' => 'ticket replied successfully'], Response::HTTP_CREATED);
    }

    /**
     * Reply Ticket
     * @method GET
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticketList()
    {
        $data = resolve(CustomerRepository::class)->ticketList();

        return response()->json(['message' => 'Ticket list',
        'data'=>TicketResource::collection($data)
        ], Response::HTTP_OK);

    }


}
