<?php

namespace App\Http\Requests;

use App\Rules\AuthValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $request = $this->request->all();

        $rules ['email'] = ['required', 'exists:users,email',new AuthValidationRule($request['email']??null,
            $request['password']??null)];
        $rules['password']=['required'];
        return $rules;


    }
}
