<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'priority' => $this->priority,
            'status' => $this->status,
            'is resolved' => $this->is_resolved,
            'category' => $this->category->name,
            'reply' => TicketMessageResource::collection($this->message)
        ];
    }
}
