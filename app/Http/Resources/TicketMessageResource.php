<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketMessageResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'message' => $this->message,
            'user'=>$this->user_id
        ];
    }
}
