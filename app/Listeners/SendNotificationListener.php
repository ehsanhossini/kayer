<?php

namespace App\Listeners;

use App\Events\SendNotificationEvent;
use App\Models\Notification;


class SendNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SendNotificationEvent  $event
     * @return void
     */
    public function handle(SendNotificationEvent $event)
    {
        $data = $event->data;
        Notification::create([
            'ticket_id'=>$data['ticket_id'],
            'message'=>$data['message']
        ]);
    }
}
