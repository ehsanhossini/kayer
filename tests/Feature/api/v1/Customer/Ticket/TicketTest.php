<?php

namespace Tests\Feature\api\v1\Customer\Ticket;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BookingTest
 */
class TicketTest extends TestCase
{
    /**
     * @param string $return_type
     * @return string[]
     */
    private function headerWithToken($return_type = 'token')
    {
        if(!$user = User::where('role',User::ROLE_CUSTOMER)->first()){
            $user = User::create([
                'name' => 'employaa',
                'email' => 'employee@gmail.com',
                'password' => bcrypt('123'),
                'mobile' => '91111',
            ]);
        }
        $tokenResult = $user->createToken(config('kayer.access_token'));

        return [
            'Authorization' => 'Bearer' . ' ' . $tokenResult->plainTextToken,
            'Accept' => 'application/json'
        ];

    }

    /** @test */
    public function customer_can_see_own_ticket()
    {
        $response = $this->withHeaders($this->headerWithToken())->get(route('customer-ticket-list'));
        $response->assertStatus(Response::HTTP_OK);
    }


}

