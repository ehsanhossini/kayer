<?php

namespace Tests\Feature\api\v1\Customer\Auth;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BookingTest
 */
class AuthenticationTest extends TestCase
{
    /** @test */
    public function login_should_be_validate()
    {
        $response = $this->postJson(route('auth-login'), []);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function login()
    {
        $response = $this->postJson(route('auth-login'), [
            "email" => 'employee1@gmail.com',
            "password" => 'employee123'
        ]);
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function register_should_be_validate()
    {
        $response = $this->postJson(route('auth-register'), [

        ]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function testDatabase()
    {
        $this->assertDatabaseHas('users', [
            'email' => 'employee1@gmail.com',
        ]);
    }

}

